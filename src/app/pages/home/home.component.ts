import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  text: string = '';

  constructor() {}

  ngOnInit(): void {}

  additionalInfo(category: string) {
    switch (category) {
      case 'school':
        this.text =
          'We did this Project in school to learn scrum. Our goal wasnt to make a mindblowing application due to our limited time.';
        break;
      case 'time':
        this.text =
          'In total we had 40 hours to learn and use scrum for this project. For some of us this was the first time working with scrum.';
        break;
      case 'happy':
        this.text =
          'We are very happy with the teamwork in the group. It was a great experience and we are more than happy with the end result.';
        break;
      default:
        this.text = '';
        break;
    }
  }
}
