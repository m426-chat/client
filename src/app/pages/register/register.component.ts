import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterDTO } from 'src/DTO/RegisterDTO';
import { UserService } from 'src/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(private userService: UserService, private router: Router) {}

  private async validateUsername(control: FormControl): Promise<any> {
    return new Promise((resolve) => {
      this.userService.CheckUsername(control.value).then((exists) => {
        resolve(control.setErrors({ usernameExists: exists }));
      });
    });
  }

  private async validateEmail(control: FormControl): Promise<any> {
    return new Promise((resolve) => {
      this.userService.CheckEmail(control.value).then((exists) => {
        resolve(control.setErrors({ emailExists: exists }));
      });
    });
  }

  //TODO: Fix error of passwords match
  private passwordsMatch(group: FormGroup): any {
    return group.controls.password.value == group.controls.passwordConfirm.value
      ? group.setErrors({ passwordsMatch: true })
      : group.setErrors({ passwordsMatch: false });
  }

  registerForm: FormGroup = new FormGroup(
    {
      email: new FormControl('', {
        validators: [Validators.required, Validators.email],
        asyncValidators: [this.validateEmail.bind(this)],
        updateOn: 'blur',
      }),
      username: new FormControl('', {
        validators: [Validators.required, Validators.minLength(4)],
        asyncValidators: [this.validateUsername.bind(this)],
        updateOn: 'blur',
      }),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      passwordConfirm: new FormControl('', [Validators.required]),
    },
    { validators: [this.passwordsMatch], updateOn: 'change' }
  );

  ngOnInit(): void {}

  onSubmit(): void {
    const user: RegisterDTO = {
      email: this.registerForm.controls.email.value,
      username: this.registerForm.controls.username.value,
      password: this.registerForm.controls.password.value,
      description: "Hello I'm a scrumbag now!",
    };

    this.userService.Register(user);
    this.router.navigate(['login']);
  }
}
