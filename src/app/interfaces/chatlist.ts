export interface Chat {
  id: number;
  title: string;
  messages: Message[];
  lastMessage: Message;
}

export interface Message {
  id: number;
  author: string;
  content: string;
  timestamp: string;
}
