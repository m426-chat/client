import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Chat, Message } from '../interfaces/chatlist';
import { BehaviorSubject, Observable } from 'rxjs';
import { io, Socket } from 'socket.io-client';
import { DefaultEventsMap } from 'socket.io-client/build/typed-events';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/services/authentication.service';

@Injectable()
export class ChatService {
  private socket?: Socket<DefaultEventsMap, DefaultEventsMap>;
  private currentMessagesSubject?: BehaviorSubject<Message[]>;

  private readonly activeChat = new BehaviorSubject<number | undefined>(1);
  readonly activeChat$ = this.activeChat.asObservable();

  constructor(private http: HttpClient, private authService: AuthenticationService) {
    this.activeChat.subscribe((chatId) => this.connectToChat(chatId));
  }

  async connectToChat(chatId: number): Promise<void> {
    if (!this.socket)
      this.socket = io('localhost:3000', { query: { token: this.authService.token } })
        .on('message', console.log)
        .on('chat:message:recieve', (message) => {
          console.log('Recieved new message:', message.author);
          this.currentMessagesSubject.next([
            ...this.currentMessagesSubject.getValue(),
            message,
          ]);
        });
    this.socket.emit('chat:connect', chatId);
  }

  async getChats(): Promise<Chat[]> {
    const chatsResponse: { chats: Chat[] } = (await this.http
      .get('http://localhost:3000/chat/list')
      .toPromise()) as any;

    this.activeChat.next(chatsResponse.chats[0].id);

    console.log(chatsResponse.chats);

    return chatsResponse.chats;
  }

  switchActiveChat(id: number): void {
    this.activeChat.next(id);
  }

  async getMessages(id: number): Promise<Observable<Message[]>> {
    console.log('This is the id' + id);

    const messageResponse: { messages: Message[] } = (await this.http
      .get('http://localhost:3000/chat/' + id)
      .toPromise()) as any;

    this.currentMessagesSubject = new BehaviorSubject<Message[]>(
      messageResponse.messages
    );
    return this.currentMessagesSubject.asObservable();
  }

  sendMessage(message: string): void {
    this.socket?.emit('chat:message:send', message);
  }

  public newChat(username: string): void {
    this.http.post(`${environment.apiUrl}/chat`, { username: username }).subscribe();
  }
}
