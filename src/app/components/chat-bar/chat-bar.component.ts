import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-chat-bar',
  templateUrl: './chat-bar.component.html',
  styleUrls: ['./chat-bar.component.scss'],
})
export class ChatBarComponent {
  messageBox = '';

  constructor(private chatService: ChatService) {}

  sendMessage(): void {
    if (!this.messageBox) return;

    this.chatService.sendMessage(this.messageBox);
    this.messageBox = '';
  }
}
