import { Component, OnInit } from '@angular/core';
import { ChatService } from 'src/app/services/chat.service';
import { AuthenticationService } from 'src/services/authentication.service';
import { Chat } from './../../interfaces/chatlist';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from 'src/services/user.service';
import { User } from 'src/models/User';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss'],
})
export class ChatListComponent implements OnInit {
  username: string = this.getUsername();
  activeChat: number;

  chats: Chat[] = [];

  newChatUsername: string;

  userData: User | undefined;

  constructor(
    private chatService: ChatService,
    private authService: AuthenticationService,
    private userService: UserService,
    private router: Router
  ) {
    chatService.activeChat$.subscribe((activeChat) => (this.activeChat = activeChat));
    this.userData = JSON.parse(localStorage.getItem('user'));
  }

  async ngOnInit(): Promise<void> {
    const chats = await this.chatService.getChats();
    this.chats = chats.map((chat) => {
      if (chat.lastMessage != undefined)
        chat.lastMessage.timestamp = new Date(
          chat.lastMessage.timestamp
        ).toLocaleDateString('de');
      return chat;
    });
    //const userData = this.userService.GetUserData(this.getUsername());
  }

  /**
   * TODO: TO BE IMPLEMENTED
   * @returns Returns the username of the user
   */
  getUsername(): string | undefined {
    return this.authService.currentUser?.username;
  }

  /**
   * The active chat gets switched in the chat service
   * @param id id of the new chat clicked
   */
  switchActiveChat(id: number): void {
    this.chatService.switchActiveChat(id);
  }

  /**
   * TODO: TO BE IMPLEMENTED
   * Updates the Settings of the user with the new inputs
   */
  updateSettings(): void {
    console.log(this.userData);
  }

  /**
   * Creates a new direct message with the wished user
   */
  newDirectMessage(): void {
    this.chatService.newChat(this.newChatUsername);
  }

  /**
   * TODO: TO BE IMPLEMENTED
   * Creates a new group chat with the wished users
   */
  newGroupChat(): void {
    alert('New Group Chat needs to be implemented!');
  }

  /**
   * TODO: TO BE IMPLEMENTED
   * Logs out the user and redirects him to the homepage
   */
  logout(): void {
    this.authService.Logout();
    this.router.navigateByUrl('/home');
  }
}

document.addEventListener('DOMContentLoaded', function () {
  var elems = document.querySelectorAll('.fixed-action-btn');
  var instances = M.FloatingActionButton.init(elems, {});
});

document.addEventListener('DOMContentLoaded', function () {
  var elems = document.querySelectorAll('.modal');
  var instances = M.Modal.init(elems, {});
});
