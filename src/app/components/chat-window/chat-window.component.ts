import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { ChatService } from 'src/app/services/chat.service';
import { AuthenticationService } from 'src/services/authentication.service';
import { Message } from './../../interfaces/chatlist';

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss'],
})
export class ChatWindowComponent {
  messages: Message[] = [];
  messagesObservable?: Observable<Message[]>;
  currentUsername = '';

  constructor(chatService: ChatService, authService: AuthenticationService) {
    chatService.activeChat$.subscribe(async (id) => {
      const observableMessages = await chatService.getMessages(id);
      observableMessages.subscribe((messages) => {
        this.messages = messages.map((message) => ({
          ...message,
          timestamp: new Date(message.timestamp).toLocaleDateString('de'),
        }));
      });
    });
    this.currentUsername = authService.currentUser.username;
  }
}

/**
 * Automaticlly scrolls to the bottom of all chat messages after the DOM has finished loading
 */
document.addEventListener('DOMContentLoaded', () => {
  // prettier-ignore
  document.getElementById('chat-window-wrapper').scrollTop = document.getElementById('chat-window-wrapper').scrollHeight;
});
