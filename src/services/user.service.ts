import { HttpClient } from '@angular/common/http';
import { importExpr } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterDTO } from 'src/DTO/RegisterDTO';
import { environment } from 'src/environments/environment';
import { User } from 'src/models/User';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  /**
   * @param user The user to be registred
   * @returns true if the user got saved into the database
   */
  public Register(user: RegisterDTO): boolean {
    this.http
      .post<RegisterDTO>(`${environment.apiUrl}/register`, user)
      .toPromise()
      .then((res) => console.log(res));
    return true;
  }

  /**
   * Checks if a given username already exists
   * @param username username to be checked
   * @returns boolean if the user already exists
   */
  public async CheckUsername(username: string): Promise<boolean> {
    let isUsed: boolean = false;
    await this.http
      .post(`${environment.apiUrl}/check/username`, { username: username })
      .toPromise()
      .then((res) => {
        isUsed = false;
      })
      .catch((err) => {
        isUsed = true;
      });
    return isUsed;
  }
  /**
   * Checks if a given email already exists
   * @param email to be checked
   * @returns boolean if the email already exists
   */
  public async CheckEmail(email: string): Promise<boolean> {
    let isUsed: boolean = false;
    await this.http
      .post(`${environment.apiUrl}/check/email`, { email: email })
      .toPromise()
      .then((res) => {
        isUsed = false;
      })
      .catch((err) => {
        isUsed = true;
      });
    return isUsed;
  }

  public async GetUserData(username: string): Promise<User | undefined> {
    return (await this.http.get(`${environment.apiUrl}/settings`).toPromise()) as User;
  }
}
