import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginDTO } from 'src/DTO/LoginDTO';
import { environment } from 'src/environments/environment';
import jwtDecode from 'jwt-decode';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  private readonly userSubject: BehaviorSubject<User | null> = new BehaviorSubject<User | null>(
    null
  );

  private get _currentUser(): User | undefined {
    return JSON.parse(localStorage.getItem('user'));
  }
  private set _currentUser(user: User) {
    this.userSubject.next(user);
  }

  /**
   * @returns the currently logged in user
   */
  public get currentUser(): User {
    return this._currentUser;
  }

  /**
   * @returns a boolean if a user is currently logged in
   */
  public get isLoggedIn(): boolean {
    const token = localStorage.getItem('token');
    if (!(token == null || token == undefined || token == '')) {
      return true;
    }
    return false;
  }
  /**
   * @returns current token from local storage
   */
  public get token(): string {
    return localStorage.getItem('token');
  }

  /**
   *
   * @param user user to login
   */
  public Login(user: LoginDTO) {
    console.log(user);
    this.http.post<any>(`${environment.apiUrl}/login`, user).subscribe((res) => {
      const userData: User = jwtDecode(res.token);
      localStorage.setItem('token', res.token);
      localStorage.setItem('user', JSON.stringify(userData));
      this._currentUser = userData;
    });
  }

  public Logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }
}
